SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `comp6002_G13_assn3` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `comp6002_G13_assn3`;

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) NOT NULL,
  `PASSWRD` varchar(20) NOT NULL,
  `CATEGORY` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_admin` (`ID`, `NAME`, `PASSWRD`, `CATEGORY`) VALUES
(1, 'jeff', 'jeff', '');

CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(200) NOT NULL ,
  `description` text NULL,
  `pagename` varchar(100) NOT NULL,
  `headerorder` int(11) NOT NULL,
  `externallink` varchar(500) NULL,
  `imgsrc` VARCHAR(100) NULL,
  PRIMARY KEY (`ID`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;